# Development guide for GitLab CI/CD templates

## Requirements for CI/CD templates

- Name the template following the `*-gitlab-ci.yml` format.
- Use valid [`.gitlab-ci.yml` syntax](../../ci/yaml/index.md). Verify it's valid
  with the [CI/CD lint tool](../../ci/lint.md).



## Templates

### ACM


```
- remote: 'https://gitlab.com/group-innovation/gitlab-ci/template/raw/main/acm-gitlab-ci.yml'
```

### All


```
- remote: 'https://gitlab.com/group-innovation/gitlab-ci/template/raw/main/all-gitlab-ci.yml'
```

### App mesh

```
- remote: 'https://gitlab.com/group-innovation/gitlab-ci/template/raw/main/app-mesh-gitlab-ci.yml'
```

### EKS Components

```
- remote: 'https://gitlab.com/group-innovation/gitlab-ci/template/raw/main/components-gitlab-ci.yml'
```

### EKS Components nonprod

```
- remote: 'https://gitlab.com/group-innovation/gitlab-ci/template/raw/main/components-nonprod-gitlab-ci.yml'
```

### EKS Components Prod

```
- remote: 'https://gitlab.com/group-innovation/gitlab-ci/template/raw/main/components-prod-gitlab-ci.yml'
```

### Docdb nonprod

```
- remote: 'https://gitlab.com/group-innovation/gitlab-ci/template/raw/main/docdb-nonprod-gitlab-ci.yml'
```

### Docdb prod

```
- remote: 'https://gitlab.com/group-innovation/gitlab-ci/template/raw/main/docdbprod-gitlab-ci.yml'
```

### EKS Cluster

```
- remote: 'https://gitlab.com/group-innovation/gitlab-ci/template/raw/main/eks-gitlab-ci.yml'
```

### EKS Cluster nonprod

```
- remote: 'https://gitlab.com/group-innovation/gitlab-ci/template/raw/main/eks-nonprod-gitlab-ci.yml'
```
### EKS Cluster Prod

```
- remote: 'https://gitlab.com/group-innovation/gitlab-ci/template/raw/main/eks-prod-gitlab-ci.yml'
```

### Health checks

```
- remote: 'https://gitlab.com/group-innovation/gitlab-ci/template/raw/main/health-checks-gitlab-ci.yml'
```

### Hosted zones root

```
- remote: 'https://gitlab.com/group-innovation/gitlab-ci/template/raw/main/hosted-zone-gitlab-ci.yml'
```

### Hosted zones

```
- remote: 'https://gitlab.com/group-innovation/gitlab-ci/template/raw/main/hosted-zone-root-gitlab-ci.yml'
```

### Hosted zones records

```
- remote: 'https://gitlab.com/group-innovation/gitlab-ci/template/raw/main/hosted-zone-gitlab-ci.yml'
```

### Linters

```
- remote: 'https://gitlab.com/group-innovation/gitlab-ci/template/raw/main/linters-gitlab-ci.yml'
```

### Multi Policy Role

```
- remote: 'https://gitlab.com/group-innovation/gitlab-ci/template/raw/main/multi-policy-role-gitlab-ci.yml'
```

### Pagerduty

```
- remote: 'https://gitlab.com/group-innovation/gitlab-ci/template/raw/main/pagerduty-gitlab-ci.yml'
```

### RDS nonprod

```
- remote: 'https://gitlab.com/group-innovation/gitlab-ci/template/raw/main/rds-nonprod-gitlab-ci.yml'
```

### RDS prod

```
- remote: 'https://gitlab.com/group-innovation/gitlab-ci/template/raw/main/rds-prod-gitlab-ci.yml'
```

### S2S VPN
```
- remote: 'https://gitlab.com/group-innovation/gitlab-ci/template/raw/main/s2s-vpn-gitlab-ci.yml'
```

### S3 Bucket
```
- remote: 'https://gitlab.com/group-innovation/gitlab-ci/template/raw/main/s3-bucket-gitlab-ci.yml'
```

### S3 Bucket nonprod
```
- remote: 'https://gitlab.com/group-innovation/gitlab-ci/template/raw/main/s3-bucket-nonprod-gitlab-ci.yml'
```

### S3 Bucket prod
```
- remote: 'https://gitlab.com/group-innovation/gitlab-ci/template/raw/main/s3-bucket-prod-gitlab-ci.yml'
```
### SES
```
- remote: 'https://gitlab.com/group-innovation/gitlab-ci/template/raw/main/ses-gitlab-ci.yml'
```

### SQS nonprod
```
- remote: 'https://gitlab.com/group-innovation/gitlab-ci/template/raw/main/sqs-nonprod-gitlab-ci.yml'
```

### SQS prod
```
- remote: 'https://gitlab.com/group-innovation/gitlab-ci/template/raw/main/sqs-prod-gitlab-ci.yml'
```
### Target Components
```
- remote: 'https://gitlab.com/group-innovation/gitlab-ci/template/raw/main/target-components-gitlab-ci.yml'
```

### Validate
```
- remote: 'https://gitlab.com/group-innovation/gitlab-ci/template/raw/main/validate-gitlab-ci.yml'
```
### VPC
```
- remote: 'https://gitlab.com/group-innovation/gitlab-ci/template/raw/main/vpc-gitlab-ci.yml'
```

### VPCE
```
- remote: 'https://gitlab.com/group-innovation/gitlab-ci/template/raw/main/vpce-gitlab-ci.yml'
```

### VPN
```
- remote: 'https://gitlab.com/group-innovation/gitlab-ci/template/raw/main/vpn-gitlab-ci.yml'
```

### TAGGING
```
- remote: 'https://gitlab.com/group-innovation/gitlab-ci/template/raw/main/tag-gitlab-ci.yml'
```
